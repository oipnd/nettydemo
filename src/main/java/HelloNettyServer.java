import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;


public class HelloNettyServer {

    public static void main(String[] args) throws InterruptedException {

        //主线程组
        EventLoopGroup parentGroup = new NioEventLoopGroup();

        //从线程组
        EventLoopGroup childGroup = new NioEventLoopGroup();

        try {
            //服务器启动类
            ServerBootstrap serverBootstrap=new ServerBootstrap();
            serverBootstrap.group(parentGroup, childGroup)
                    .channel(NioServerSocketChannel.class)      //设置channel类型
                    .childHandler(new HelloServerInitinalize());  //设置子handler(自定义）
            ChannelFuture channelFuture=serverBootstrap.bind(8080).sync();
            channelFuture.channel().closeFuture().sync();
        } finally {
            parentGroup.shutdownGracefully();
            childGroup.shutdownGracefully();

        }

    }
}

