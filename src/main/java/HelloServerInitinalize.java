import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

import java.nio.channels.Pipe;

/**
 * @ClassName HelloServerInitinalize
 * @Description 初始化器，channel注册后会执行里面的初始化方法。
 * @Author yyy
 * @Date 2019/1/8 1:52
 * @Version 1.0
 **/
public class HelloServerInitinalize extends ChannelInitializer<SocketChannel> {
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        //获得对应的pipeline
        ChannelPipeline channelPipeline=socketChannel.pipeline();

        //http解码器添加
        channelPipeline.addLast("HttpServerCodec",new HttpServerCodec());

        //自定义handler添加
        channelPipeline.addLast("customeHandler",new CustomHandler());



    }
}
